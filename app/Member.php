<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
  protected $fillable = array( 'first_name', 
                               'last_name', 
                               'email',
                               'address_1',
                               'address_2',
                               'city', 
                               'region',
                               'country',
                               'phone',
                               'description',
                               'image',
                               'date_joined',
                               'active' );
  
  public function scopeActive($query)
  {
    $query->where('active', 1);
  }
    //
}
