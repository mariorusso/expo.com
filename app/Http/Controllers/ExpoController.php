<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExpoController extends Controller
{
   public function index()
  {  
    $title = 'WELCOME!!!';
    $call_to_action = 'Enjoy our major SALE!!!';
    $banner = '<img src="/assets/images/home_banner.jpg" alt="home banner" />';
    return view('pages.home', compact('title', 'banner', 'call_to_action'));
  }
  
   public function about()
  {  
    $title = 'About Us';
    $content = '<h1>HTML Ipsum Presents</h1>
	       
          <p>           
            <strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor                    quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi                  vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum              sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci,                sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.
          </p>

          <h2>Header Level 2</h2>

          <ol>
             <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
             <li>Aliquam tincidunt mauris eu risus.</li>
          </ol>

          <blockquote>
            
            <p>
            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget                          ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at                              luctus turpis elit sit amet quam. Vivamus pretium ornare est.
            </p>
              
          </blockquote>

          <h3>Header Level 3</h3>

          <ul>
             <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
             <li>Aliquam tincidunt mauris eu risus.</li>
          </ul>

          <pre><code>
          #header h1 a { 
              display: block; 
              width: 300px; 
              height: 80px; 
          }
          </code></pre>';
    return view('pages.default', compact('title', 'content'));
  }
  
  public function team()
  {  
    $title = 'Our Team';
    $content = '<img src=/assets/images/team.jpg class="pg_img" alt="team page image"> 
                <p> Lorem ipsum dolor sit amet, consecteturadipiscing elit. Suspendisse augue ex, eleifend vulputate viverra suscipit, ultrices pellentesque velit. Pellentesque pellentesque augue sit amet elit ullamcorper faucibus dapibus sed nunc. Curabitur venenatis enim quis euismod placerat. Sed id ex auctor, consequat diam a, posuere ex. Mauris pretium pretium pulvinar. In eu vehicula tellus. Aliquam erat volutpat. Cras interdum, leo sit amet laoreet efficitur, justo ligula fringilla ante, sit amet tristique dolor ipsum vel eros. Etiam placerat felis vitae euismod blandit. Donec dignissim eros massa, id pretium leo gravida quis. Vivamus malesuada nibh id massa aliquet tristique. Maecenas nibh mi, mattis ac dapibus a, fermentum non lorem. Curabitur quis hendrerit risus. Curabitur at efficitur lorem. Sed ut lorem id massa placerat molestie sit amet sit amet augue. Integer vitae eros nisl.</p>

<p>Phasellus et dolor vitae magna elementum sollicitudin. Morbi at ullamcorper dui. Vestibulum vel sem sagittis lacus sodales finibus quis non purus. Aliquam erat volutpat. Nullam justo orci, euismod eget dui a, cursus viverra metus. Nunc elementum justo sodales velit semper bibendum. Vestibulum ac nulla in nunc bibendum interdum. Duis imperdiet, turpis non pharetra porta, tellus enim feugiat eros, ut efficitur nunc dolor ac eros. Maecenas lorem mauris, efficitur et diam rhoncus, malesuada luctus purus. Pellentesque tortor turpis, tempus dapibus auctor eu, fermentum eget mauris. Mauris cursus dignissim neque, in ornare velit ornare convallis. Praesent lectus sem, dignissim et semper ac, aliquet et nisl. In sit amet tempus justo. Sed enim augue, tempor a orci non, fringilla tristique sapien. Sed at felis nec leo tincidunt scelerisque. Aenean at dolor non mauris finibus consectetur ut quis massa.  ';
    return view('pages.default', compact('title', 'content'));
  }
  
  public function contact()
  {  
    $title = 'Contact Us';
    $content = '<div class="col-xs-12 col-sm-6">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2570.4084252234657!2d-97.15306435!3d49.891135049999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52ea73e4add9fbbb%3A0x9b84b0accf63fdb0!2sUniversity+of+Winnipeg!5e0!3m2!1spt-BR!2sca!4v1437695519557" width="600" height="450" style="border:0" allowfullscreen>
    
  </iframe>
  
</div>

<div class="col-xs-12 col-sm-6">
  <h3>Let\'s Talk</h3>
  <p class="contact">
    phone: 204 222 3344<br />
    email: expo@expo.com<br />
    We are located at: 460 Portage Ave. Winnipeg MB<br />
  </p>
</div>';
    return view('pages.default', compact('title', 'content'));
  }
  
  public function shop()
  {  
    $title = 'Shop';
    return view('pages.shop', compact('title'));
  }
  
  public function terms()
  {  
    $title = 'Terms and Conditions';
    $content = '<ul>
       <li>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam.            Sedarcu. Cras consequat.
       </li>
       <li>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat.              Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.
       </li>
       <li>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique                cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.
       </li>
       <li>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate,                nunc.      
      </li>
    </ul>';
    return view('pages.terms', compact('title', 'content'));
  }
  
  public function privacy()
  {  
    $title = 'Privacy Statement';
    $image = '<img src="/assets/images/private.jpg" alt="privacy image"/>';
    $content = '<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
   <li>Vestibulum auctor dapibus neque.</li>
    </ol>';
    return view('pages.privacy', compact('title', 'image', 'content'));
  }
  
  public function careers()
  {  
    $title = 'Careers';
    $content = 'At this time we have some positions available, please refer to the list below.';
    $positions = array('E-commerce Assitant', 'Marketing Manager', 'SEO Specialist', 'Recepcionist');
    $image = '<img src="http://cdn2.hubspot.net/hub/315483/file-2465997356-png/we-are-hiring.png" alt="we are hiring image"/>';
    return view('pages.careers', compact('title', 'content', 'positions', 'image'));
  }
  
  
}
