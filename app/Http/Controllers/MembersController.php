<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    public function index()
    {
        $title = 'Members';
        $members = \App\Member::latest()->active()->get()->toArray();
        return view ('members/member_list', compact('title', 'members'));
    }

    public function show($id)
    {
         $title = 'Member Detail';
        $member =  \App\Member::findOrFail($id)->toArray();
        return view ('members/member_single', compact('title', 'member'));
    }
}
