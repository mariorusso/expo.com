<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/* Pages Routes */

Route::get('/', 'ExpoController@index');
Route::get('about', 'ExpoController@about');
Route::get('team', 'ExpoController@team');
Route::get('contact', 'ExpoController@contact');
Route::get('careers', 'ExpoController@careers');
Route::get('terms', 'ExpoController@terms');
Route::get('privacy', 'ExpoController@privacy');
Route::get('shop', 'ExpoController@shop');

/* Members Routes */

Route::get('members', 'MembersController@index');
Route::get('members/{id}', 'MembersController@show');
