@extends('master')

@section('content')
<h1 class="title">{{ $title }}</h1>

<div class="row">
  @foreach($members as $member)
        <div class="col-xs-12 col-sm-3 member">

        <div class="photo">
        @if(!$member['image'] || $member['image'] == ' ')
          {!! HTML::image('/assets/images/placeholder.jpg', $member['first_name'],  array('class' => 'member_photo', 'height' => 200, 'width' => 200)) !!}
        @else
          {!! HTML::image( $member['image'], $member['first_name'],  array('class' => 'member_photo', 'height' => 200, 'width' => 200)) !!}
        @endif
        </div><!-- END of photo -->

        <div class="name">
          <p>{{ $member['first_name'] . ' '  . $member['last_name'] }}</p>
        </div><!-- END of name -->

        <div class="phone">
          <p>ph: {{ $member['phone'] }}</p>      
        </div><!-- END of price -->
         <div class="prod_button">
            <a href="/members/{{$member['id']}}"  class="btn btn-default" role="button">
             
                Detail
             
            </a>
        </div>
      </div><!-- END of Member -->
  @endforeach  
  
</div><!-- END of row -->
@stop('content')