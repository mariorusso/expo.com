@extends('master')

@section('content')
<h1 class="title">{{ $title }}</h1>

<div class="row">
 
        <div class="col-xs-12 member">

        <div class="col-xs-12 col-sm-4 photo">
        @if(!$member['image'] || $member['image'] == ' ')
          {!! HTML::image('/assets/images/placeholder.jpg', $member['first_name'],  array('class' => 'member_photo', 'height' => 200, 'width' => 200)) !!}
        @else
          {!! HTML::image( $member['image'], $member['first_name'],  array('class' => 'member_photo', 'height' => 200, 'width' => 200)) !!}
        @endif
        </div><!-- END of photo -->

        <div class="col-xs-12 col-sm-4 info">
            <h3>Name:</h3>
            <p>{{ $member['first_name'] . ' '  . $member['last_name'] }}</p>
            
            <h3>Phone:</h3>
            <p>{{ $member['phone'] }}</p>
          
            <h3>Email:</h3>
            <p>{{ $member['email'] }}</p>
            
            <h3>Address:</h3>
            <p>{{ $member['address_1'] }} {{ $member['address_2'] }}<br />
               {{ $member['city'] }} - {{ $member['region'] }} - {{ $member['country'] }} </p>
          
            <h3>Description:</h3>
            <p>{{ $member['description'] }}</p>
        </div><!-- END of info -->
         
      </div><!-- END of Member -->

  
</div><!-- END of row -->
@stop('content')