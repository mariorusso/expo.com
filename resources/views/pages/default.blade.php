@extends('master')

@section('content')

<h1 class="title">{{ $title }}</h1>

<div class="row">
  <div id="primary" class="col-xs-12">
   
      {!! $content !!}
    
    
  </div>
  
</div>

      

      
@stop('content')