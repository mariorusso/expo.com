@extends('master')

@section('content')
<h1 class="title">{{ $title }}</h1>

<div class="row">
  <div class="col-xs-12 col-sm-3 product">
    
    <div class="photo">
      <img src="/assets/images/gold.jpg" alt="gold bracelet" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Gold Bracelet</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$50.00</p>      
    </div><!-- END of price -->
     <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  <div class="col-xs-12 col-sm-3 product">
    
    <div class="photo">
      <img src="/assets/images/gold_ear.jpg" alt="gold earing" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Gold Earring</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$95.00</p>      
    </div><!-- END of price -->
     <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  <div class="col-xs-12 col-sm-3 product">
    
    <div class="photo">
      <img src="/assets/images/gold_neck.jpg" alt="gold Necklace" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Gold Necklace</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$105.00</p>      
    </div><!-- END of price -->
     <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  <div class="col-xs-12 col-sm-3 product">
    
    <div class="photo">
      <img src="/assets/images/gold_pend.jpg" alt="gold pendant" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Gold Pendant</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$20.00</p>      
    </div><!-- END of price -->
     <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
</div><!-- END of row -->

<div class="row">
  <div class="col-xs-12 col-sm-3 product">
    
    <div class="photo">
      <img src="/assets/images/silver.jpg" alt="Silver bracelet" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Silver Bracelet</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$35.00</p>      
    </div><!-- END of price -->
     <div>
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  <div class="col-xs-12 col-sm-3 product">
    
    <div class="photo">
      <img src="/assets/images/silver_ear.jpg" alt="silver earring" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Silver Earring</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$30.00</p>      
    </div><!-- END of price -->
     <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  <div class="col-xs-12 col-sm-3 product">
    
    <div class="photo">
      <img src="/assets/images/silver_neck.jpg" alt="silver necklace" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Silver Necklace</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$90.00</p>      
    </div><!-- END of price -->
     <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  <div class="col-xs-12 col-sm-3 product">
    
    <div class="photo">
      <img src="/assets/images/silver_pend.jpg" alt="silver pendant" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Silver Pendant</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$20.00</p>      
    </div><!-- END of price -->
    <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  
</div><!-- END of row -->
@stop('content')