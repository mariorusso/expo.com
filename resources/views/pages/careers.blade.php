@extends('master')

@section('content')
<h1 class="title">{{ $title }}</h1>

<div class="row">
  <div id="primary" class="col-xs-12 col-sm-6">
    <p class='intro'>
      {!! $content !!}
    </p>
    
    <ul>
      
      @foreach($positions as $position)
      
        <li>{{ $position }}</li>
      
      @endforeach
      
    </ul>
    
    <p>
      If you have any interest in some position listed, feel free to <a href="/contact">contact us</a>. 
    </p>
  </div>

  <div id="secondary" class="col-xs-12 col-sm-6">
    {!! $image !!}
  </div>
</div>
@stop('content')