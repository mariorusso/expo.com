@extends('master')

@section('content')
<div class="row">
  <div id="home_banner">
    {!! $banner !!}
  </div>
</div>

<div class="row">
  <div id="primary" class="col-xs-12">
    <p class="callout">
      {!! $call_to_action !!}
    </p>    
  </div>
</div>

<div class="row">
  <div class="col-xs-12 col-sm-4 product">
    
    <div class="photo">
      <!-- image using laravel/blade pattern -->
      {!! HTML::image('/assets/images/silver_pend.jpg', 'silver pendant') !!} 
      <!--<img src="/assets/images/silver_pend.jpg" alt="silver pendant" width="250" height="250" />-->
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Silver Pendant</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$20.00</p>      
    </div><!-- END of price -->
    <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  <div class="col-xs-12 col-sm-4 product">
    
    <div class="photo">
      <img src="/assets/images/gold_neck.jpg" alt="gold Necklace" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Gold Necklace</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$105.00</p>      
    </div><!-- END of price -->
     <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
  
  <div class="col-xs-12 col-sm-4 product">
    
    <div class="photo">
      <img src="/assets/images/gold_pend.jpg" alt="gold pendant" width="250" height="250" />
    </div><!-- END of photo -->
    
    <div class="name">
      <p>Gold Pendant</p>
    </div><!-- END of name -->
    
    <div class="price">
      <p>$20.00</p>      
    </div><!-- END of price -->
     <div class="prod_button">
      <button class="btn btn-default">BUY</button>
    </div>
  </div><!-- END of product -->
</div>
      

      
@stop('content')