<!DOCTYPE html>

<html>
	<head>
		<title>{{ $title }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Electrolize' rel='stylesheet' type='text/css'>
		{!! HTML::style('assets/css/style.css') !!}
	</head>
	<body>
		<div class="container-fluid">

		  <div class="row">
		  	
		    <nav id="topnav" class="navbar navbar-default">
		      <div class="navbar-header">
		      	 
		      	<button type="button" class="navbar-toggle" data-toggle="collapse" 
		        		data-target="#my-navbar-collapse">
		         	<span class="icon-bar"></span>
		         	<span class="icon-bar"></span>
		         	<span class="icon-bar"></span>
		      	</button>
		      
				<a class="navbar-brand">
					EXPO.com
		        </a>
		     </div>
			     <div class="collapse navbar-collapse" id="my-navbar-collapse">
			      	<ul id="menu-navbar" class="nav navbar-nav">
			      		<li>{!! HTML::link('/', 'Home') !!}</li>
						<li>{!! HTML::link('/about', 'About') !!}</li>
						<li>{!! HTML::link('/shop', 'Shop') !!}</li>
						<li>{!! HTML::link('/team', 'Team') !!}</li>
						<li>{!! HTML::link('/members', 'Members') !!}</li>
                        <li>{!! HTML::link('/contact', 'Contact Us') !!}</li>
					</ul>     
				</div>
		    </nav>
		
		  </div>
		
		</div><!-- /Navbar -->
		