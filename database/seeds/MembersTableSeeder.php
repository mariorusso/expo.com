<?php

use Illuminate\Database\Seeder;
use App\Member;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('members')->delete();
      
      Member::create([
        'first_name' =>  'Mario',
        'last_name' => 'Russo',
        'email' => 'mariorusso@gmail.com',
        'address_1' => '3 Donald St.',
        'address_2' => 'Apartment 1803',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '204 330 3249',
        'description' => 'Very nice guy!!!',
        'image' => '/assets/images/mario.jpg',
        'date_joined' => '2015-07-01',
        'active' => true ]);
      
      Member::create([
        'first_name' =>  'Luciano',
        'last_name' => 'Russo',
        'email' => 'lr@gmail.com',
        'address_1' => '490 Al. Garuja',
        'city' => 'Sao Paulo', 
        'region' => 'SP',
        'country' => 'Brasil',
        'phone' => '55 11 999 9999',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'image' => '/assets/images/luciano.jpg',
        'date_joined' => '2014-10-22',
        'active' => true ]);
      
      Member::create([
        'first_name' =>  'Mike',
        'last_name' => 'Tyson',
        'email' => 'mkty@gmail.com',
        'address_1' => '3 Carlson St.',
        'address_2' => '2003',
        'city' => 'Las Vegas', 
        'region' => 'NV',
        'country' => 'United States',
        'phone' => '1 222 333 4444',
        'description' => 'Use to be a very good fighter',
        'image' => '/assets/images/mkty.jpg',
        'date_joined' => '2015-06-22',
        'active' => false ]);
      
      Member::create([
        'first_name' =>  'Jose',
        'last_name' => 'Carlos',
        'email' => 'jc@gmail.com',
        'address_1' => '5 River Ave.',
        'address_2' => 'Apartment 3',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '111 222 3333',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'date_joined' => '2015-05-01',
        'active' => true ]);
      
       Member::create([
        'first_name' =>  'Chris',
        'last_name' => 'Stafo',
        'email' => 'cst@gmail.com',
        'address_1' => '23 Portage Ave.',
        'address_2' => 'Apartment 14',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '111 222 9999',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'image' => '/assets/images/cs.jpg',
        'date_joined' => '2015-06-01',
        'active' => true ]);
      
       Member::create([
        'first_name' =>  'John',
        'last_name' => 'Silva',
        'email' => 'jscv@gmail.com',
        'address_1' => '23 Wellington Rd.',
        'address_2' => '1023',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '111 333 4444',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'image' => ' ',
        'date_joined' => '2015-04-10',
        'active' => false ]);
      
       Member::create([
        'first_name' =>  'Steffen',
        'last_name' => 'Sturve',
        'email' => 'steffen@gmail.com',
        'address_1' => '5 River Ave.',
        'address_2' => 'Apartment 49',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '111 555 3333',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'image' => '/assets/images/steffen.jpg',
        'date_joined' => '2015-03-24',
        'active' => true ]);
      
       Member::create([
        'first_name' =>  'Chloe',
        'last_name' => 'Rivera',
        'email' => 'chr@gmail.com',
        'address_1' => '1095 Osborne St.',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '111 111 9999',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'image' => '/assets/images/chloe.jpg',
        'date_joined' => '2015-05-01',
        'active' => false ]);
      
       Member::create([
        'first_name' =>  'Salt',
        'last_name' => 'John',
        'email' => 'salt@gmail.com',
        'address_1' => '1234 Globe St.',
        'address_2' => 'Apartment 43',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '111 456 3333',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'image' => ' ',
        'date_joined' => '2015-05-28',
        'active' => true ]);
      
       Member::create([
        'first_name' =>  'Tae',
        'last_name' => 'Tio',
        'email' => 'tae@gmail.com',
        'address_1' => '90 Harvard Ave.',
        'address_2' => 'Apartment 390',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '111 222 8973',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'image' => '/assets/images/tae.jpg',
        'date_joined' => '2015-03-17',
        'active' => false ]);
      
      Member::create([
        'first_name' =>  'Tony',
        'last_name' => 'Dram',
        'email' => 'drama@gmail.com',
        'address_1' => '900 Drama Ave.',
        'address_2' => 'Apartment 999',
        'city' => 'Winnipeg', 
        'region' => 'MB',
        'country' => 'Canada',
        'phone' => '111 999 9999',
        'description' => 'Lorem Ipsum, Ipsun Lorem',
        'date_joined' => '2014-09-20',
        'active' => true ]);
      
    }
}
